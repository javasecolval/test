/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.colval.expedia.model;

/**
 *
 * @author rouanehm
 */
public class Client {
    private final int id;
    private final String name;

    public Client(int id, String name) {
        this.id = id;
        this.name = name;
    }
    
}
